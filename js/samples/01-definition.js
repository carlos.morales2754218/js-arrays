/**
 * Definición de un array en JS
 */
var runner = function () {
  // Definir un arreglo vacio
  let emptyArray = []
  console.log(
    'Se puede definir un arreglo vacío usando corchetes:\n',
    emptyArray
  )

  // Definir arreglos con sus elementos
  let numbersArray = [1, 2, 3, 4, 5]
  let stringsArray = ['uno', 'dos', 'tres']
  console.log(
    'Los arreglos, generalmente se construyen con elementos del mismo tipo',
    '\nPor ejemplo un arreglo de valores numéricos:\n',
    numbersArray,
    '\n\nO un arreglo de valores de cadenas de caracteres:\n',
    stringsArray
  )
}