/**
 * Arreglos asociativos
 */

var runner = function () {
  // Crear arreglos asociativos
  let assocArray = { 'uno': 34, 'dos': 45, 'tres': 37 }
  console.log(
    'Propiamente en JS, no se crean arreglos asociativos, sin embargo, es posible tener ',
    'una aproximación al concepto mediante los objetos. En este caso, definiendolo no entre ',
    'corchetes, sino entre llaves:\n',
    assocArray
  )

  // Acceso a los elementos del arreglo
  let selectedKey = 'uno'
  let assocArrayElement = assocArray[selectedKey]
  console.log(
    'Para acceder a los elementos de un arreglo asociativo, es igual usando los corchetes, ',
    'pero adicionalmente, se una la \'key\' definida para el elemento en lugar de un índice.',
    '\n\nAsí por ejemplo si se tiene el siguiente arreglo:\n',
    assocArray,
    `\n\nSe puede acceder al elemento con key ['${selectedKey}'] y obtener su valor:`,
    assocArrayElement
  )
}