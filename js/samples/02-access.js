/**
 * Acceso a los elementos de un arreglo
 */

var runner = function () {
  // Tamaño de un arreglo
  let sampleArray = [4, 6, 7, 12, 5, 1]
  let sampleArrayLength = sampleArray.length
  console.log(
    'Los arreglos tienen un tamaño, y este se puede acceder mediante la propiedad \'length\'',
    '\nAsí por ejemplo si se tiene el arreglo:\n',
    sampleArray,
    `\n\nAl contar los elementos del mismo se tendrían ${sampleArrayLength} elementos`
  )

  // Elementos individuales en un arreglo
  let sampleArrayElementPosition = 2
  let sampleArrayElement = sampleArray[sampleArrayElementPosition]
  let sampleArrayFirstElement = sampleArray[0]
  let sampleArrayLastElement = sampleArray[sampleArrayLength - 1]
  let sampleArrayElementInvalidPosition = sampleArrayLength + 5;
  let sampleArrayInvalidElement = sampleArray[sampleArrayElementInvalidPosition]
  console.log(
    'Los arreglos en JS son de \'índice 0\', esto significa que sus elementos se encuentran enumerados empezando con el 0',
    '\n\nPara acceder a los elementos en un arreglo, basta con indicar entre corchetes la posición del elemento en cuestion',
    '\n\nAsí pues para el arreglo de ejemplo\n',
    sampleArray,
    `\n\nLa posición [${sampleArrayElementPosition}] corresponde al elemento número ${sampleArrayElementPosition + 1} del arreglo:`,
    sampleArrayElement,
    '\n\nLa posición [0] corresponde al primer elemento del arreglo:',
    sampleArrayFirstElement,
    `\n\nLa posición [${sampleArrayLength - 1}] corresponde al último elemento del arreglo:`,
    sampleArrayLastElement,
    `\n\nEn caso de una posición fuera de los límites del arreglo como la [${sampleArrayElementInvalidPosition}] resultaría en un valor:`,
    sampleArrayInvalidElement
  )

  // Modificar elementos del arreglo
  let sampleArrayCopy = [...sampleArray]
  let positionToChange = 3
  let newValue = 5
  sampleArrayCopy[positionToChange] = newValue
  console.log(
    'Es posible modificar el valor de un elemento en un arreglo simplemente asignandolo a su posición',
    '\n\nSi tenemos el siguiente arreglo\n',
    sampleArray,
    `\n\nY se modifica el valor en la posición [${positionToChange}] y se asigna como nuevo valor el ${newValue}, entonces el arreglo quedaría de la siguiente manera:\n`,
    sampleArrayCopy
  )
}
